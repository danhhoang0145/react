// function array() {
//   const arr = [1, 2, 3, 7, 5, 6, 4, 8, 9, 10];

//   console.log("Các phần tử của mảng: ");
//   for (let i = 0; i < arr.length; i++) {
//     console.log(arr[i]);
//   }
//   return arr;
// }
// console.log("Mảng vòng lặp for [" + array().join(" , ") + "]");

// console.log("Các phương thức prototype của mảng");

// //do length là 1 thuộc tính
// function arrayLength() {
//   const arr = [1, 2, 3, 7, 5, 6, 4, 8, 9, 10];
//   return arr.length;
// }
// console.log("Độ dài của mảng " + arrayLength());

// //pop là 1 phương thức xử lí
// function arrayPop() {
//   const arr = [1, 2, 3, 7, 5, 6, 4, 8, 9, 10];
//   arr.pop();
//   console.log("Xoá 1 phần tử cuối trong mảng: [" + arr.join(" , ") + "]");
// }
// arrayPop();

// //push là thêm cuối mảng nhưng hãy cho nó 1 phần tử bất kì
// function arrayPush() {
//   const arr = [1, 2, 3, 7, 5, 6, 4, 8, 9, 10];
//   arr.push(16);
//   console.log("Thêm 1 phần tử cuối trong mảng: [" + arr.join(",") + "]");
// }
// arrayPush();

// //xoá 1 phần tử vào đầu mảng
// function arrayShift() {
//   const arr = [1, 2, 3, 7, 5, 6, 4, 8, 9, 10];
//   arr.shift();
//   console.log("Xoá 1 phần tử đầu trong mảng: [" + arr.join(",") + "]");
// }
// arrayShift();

// //hãy truyền cho nó 1 phần tử bất kì để nó đưa vào đầu mảng
// function arrayUnShift() {
//   const arr = [1, 2, 3, 7, 5, 6, 4, 8, 9, 10];
//   //arr.unshift(0)
//   //console.log("Thêm 1 phần tử đầu trong mảng: [" + arr.join(',')+ "]")
//   arr.unshift(3);
//   console.log(arr);
// }
// arrayUnShift();

// function arrayConcat() {
//   const arr = [1, 2, 3, 7, 5, 6, 4, 8, 9, 10];
//   const arr1 = [1, 2, 3, 7];
//   const arrNew = arr.concat(arr1);
//   console.log(arrNew);
// }
// arrayConcat();

// //nó đang sắp xếp theo thứ tự từ 1 đén 9 và nó sẽ chạy lại 1 lần nữa nếu là 11
// //nó sẽ đưa lên đứng thứ 2
// function arraySort() {
//   const arr = [1, 2, 3, 7, 5, 6, 4, 8, 9, 11];
//   arr.sort((a, b) => a - b);
//   console.log(arr);
// }
// arraySort();

// //trong phương thức đó là
// //5 sẽ là vị trí phần tử
// //2 sẽ là xoá 2 phần tử
// //10 sẽ là thêm vào trí đã xoá
// function arraySplice() {
//   const arr = [1, 2, 3, 7, 5, 6, 4, 8, 9, 11];
//   console.log(arr.splice(5, 2, 10));
// }
// arraySplice();

// //lấy ra mảng còn dựa vàod đầu và cuối
// //2 là sẽ đếm vị trí số 2 của mảng
// //5 là vị trí cuối
// function arraySlice() {
//   const arr = [1, 2, 3, 4, 5, 6];
//   console.log(arr.slice(2, 5));
// }
// arraySlice();

//Hàm kiểm tra số nguyên dượng
// kiểm trả đièu kiện > 0
// phải là số nguyên
function laSoNguyenDuong(value) {
  return Number.isInteger(value) && value > 0;
}
console.log(laSoNguyenDuong(1));
console.log(laSoNguyenDuong(1.2));
console.log(laSoNguyenDuong(3));
console.log(laSoNguyenDuong(0));

//đây là dạng arrow function
//nó sẽ kết hợp 2 điều kiện
//nếu giá trị nó phải lớn hơn 0 và phải là số nguyên
const laSoNguyenDuong1 = (value) => {
  return Number.isInteger(value) && value > 0;
};
console.log(laSoNguyenDuong1(1));
console.log(laSoNguyenDuong1(1.2));
console.log(laSoNguyenDuong1(3));
console.log(laSoNguyenDuong1(0));

//kiểm tra theo dạng toán tử chia lấy dư
//nhược điêm cái này là ở chỗ nếu truyền vào chuỗi là số nguyên dương thì nó vẫn trả về true
const laSoNguyenDuong2 = (value) => {
  return value > 0 && value % 1 == 0;
};
console.log(laSoNguyenDuong2(1));
console.log(laSoNguyenDuong2(1.2));
console.log(laSoNguyenDuong2(3));
console.log(laSoNguyenDuong2("1"));

//Cái này là phải bắt cho nhập 1 số
let laSoNguyenDuong3 = (value) => {
  return typeof value == "number" && value > 0 && value % 1 == 0;
};
console.log(laSoNguyenDuong3(1));

//có thẻ nhập chuỗi số
function laSoNguyenAm(value) {
  return Number.isInteger(value) && value < 0;
}
console.log(laSoNguyenAm(-1));

//ở đây là không cho nhập chuỗi số
const laSoNguyenAm1 = (value) => {
  return typeof Number.isInteger == "number" && value < 0 && value % 1 == 0;
};
console.log(laSoNguyenAm(-1));

//tính tổng 2 số bằng java
//dùng let có thể thay đổi giá trị của nó
let a = 20;
let b = 30;
// a = 21;

let tong = a + b;

console.log("Tổng số là: " + tong);

//đây là hàm tính tổng bằng function
//parseInt chuyển đổi thành số nguyên
//result.innerHTML dùng để thay thế id có chữ là result
function sum() {
  let a = Number(document.getElementById("num1").value);
  let b = Number(document.getElementById("num2").value);
  sum = parseInt(a) + b;
  document.getElementById("result").innerHTML = sum;
}

//dạng muốn tính tổng trong 1 ojb
let numbers = {
  number1: 10,
  number2: 30,
};
let sum2 = numbers.number1 + numbers.number2;
console.log(sum2);

//kiểm tra dạng dữ liệu trước khi tính
function sum3() {
  let a = Number(document.getElementById("num3").value);
  let b = Number(document.getElementById("num4").value);

  if (a == "" || b == "") {
    alert("Vui lòng nhập vào hai số");
    return false;
  }

  a = Number(a);
  b = Number(b);

  if (isNaN(a) || isNaN(b)) {
    alert("Bạn phải nhập vào hai số");
    return false;
  }

  let sum = parseInt(a) + b;

  document.getElementById("resultt").innerHTML = sum;
}

//Hàm không validate
//hàm này nghĩa là nó không được truyền dữ liêu vào nên nó sẽ không hiện
//giá trị

function sum4(a, b) {
  return a + b;
}
console.log(sum4(2, 2));

// Hoặc dùng arrow function
let sum5 = (a, b) => {
  return a + b;
};
console.log(sum5(6, 6));

//Hàm Có validate dữ liệu
function sum6(a, b) {
  a = Number(a);
  b = Number(b);
  if (isNaN(a) || isNaN(b)) {
    return false;
  }
  return a + b;
}
console.log(sum5(3, 3));

// Hoặc dùng arrow function
let sum8 = (a, b) => {
  a = Number(a);
  b = Number(b);
  if (isNaN(a) || isNaN(b)) {
    return false;
  }
  return a + b;
};
console.log(sum8(1, 1));

//dùng thêm try catch để bắt lỗi
//nếu nhập chuỗi thì sẽ bị bắt lỗi err

function sum9(a, b) {
  try {
    a = Number(a);
    b = Number(b);
    if (isNaN(a) || isNaN(b)) {
      throw new Error("Dữ liệu bạn nhập vào không phải là số");
    }
    return a + b;
  } catch (e) {
    console.log(e.message);
    return false;
  }
}
console.log(sum9(7, -7));

//tính tổng bên trong mảng

function sumArray(mang1) {
  let sum = 0;
  for (let i = 0; i < mang1.length; i++) {
    sum += mang1[i];
  }
  return sum;
}
let mang1 = [1, 2, 3, 4];
console.log(sumArray(mang1));

//dùng bằng vòng lập while
function sumArray(mang2) {
  let sum = 0;
  let i = 0;
  while (i < mang2.length) {
    sum += mang2[i];
    i++;
  }
  return sum;
}
let mang2 = [1, 5, 9, 10];
console.log(sumArray(mang2));

//dùng forEach để duyệt
function sumArray(mang3) {
  let sum = 0;
  mang3.forEach(function (value) {
    sum += value;
  });
  return sum;
}
let mang3 = [4, 5, 6, 7];
console.log(sumArray(mang3));

//mai anh giảng lại
function sumArray(mang) {
  let sum = 0;
  mang.map(function (value) {
    sum += value;
  });
  return sum;
}
let mang = [1, 1, 1, 1];
console.log(sumArray(mang));

//kiểm tra số nguyên tố dạng 1
//số nguyên tố là số lớn hơn 1 và chia hết cho 1 và chính nó
function kiem_tra_snt(n) {
  var flag = true;
  // Nếu n bé hơn 2 tức là không phải số nguyên tố
  if (n < 2) {
    flag = false;
  } else {
    // lặp từ 2 tới n-1
    for (var i = 2; i < n - 1; i++) {
      if (n % i == 0) {
        flag = false;
        break;
      }
    }
  }

  // Kiểm tra biến flag
  if (flag == true) {
    document.write(n + " là số nguyên tố <br/>");
  } else {
    document.write(n + " không phải là số nguyên tố <br/>");
  }
}
kiem_tra_snt(1);
kiem_tra_snt(2);
kiem_tra_snt(3);
kiem_tra_snt(4);
kiem_tra_snt(5);

//kiểm tra số nguyên tố dạng 2
//số 2 là số nguyên tố chẵn duy nhất
function kiem_tra_snt(n) {
  var flag = true;
  // Nếu n bé hơn 2 tức là không phải số nguyên tố
  if (n < 2) {
    flag = false;
  } else if (n == 2) {
    flag = true;
  } else if (n % 2 == 0) {
    flag = false;
  } else {
    // lặp từ 3 tới n-1 với bước nhảy là 2 (i+=2)
    for (var i = 3; i < n - 1; i += 2) {
      if (n % i == 0) {
        flag = false;
        break;
      }
    }
  }
  // Kiểm tra biến flag
  if (flag == true) {
    document.write(n + " là số nguyên tố <br/>");
  } else {
    document.write(n + " không phải là số nguyên tố <br/>");
  }
}
kiem_tra_snt(6);
kiem_tra_snt(7);
kiem_tra_snt(8);
kiem_tra_snt(9);
kiem_tra_snt(10);

//kiểm tra dạng 3
// là số nguyên tố thì trong khoảng từ 2 đến căn bậc hai cua n sẽ không tồn tại số mà n chia hết
function kiem_tra_snt(n) {
  var flag = true;
  // Nếu n bé hơn 2 tức là không phải số nguyên tố
  if (n < 2) {
    flag = false;
  } else if (n == 2) {
    flag = true;
  } else if (n % 2 == 0) {
    flag = false;
  } else {
    // lặp từ 3 tới n-1 với bước nhảy là 2 (i+=2)
    for (var i = 3; i < Math.sqrt(n); i += 2) {
      if (n % i == 0) {
        flag = false;
        break;
      }
    }
  }
  // Kiểm tra biến flag
  if (flag == true) {
    document.write(n + " là số nguyên tố <br/>");
  } else {
    document.write(n + " không phải là số nguyên tố <br/>");
  }
}

kiem_tra_snt(11);
kiem_tra_snt(12);
kiem_tra_snt(13);
kiem_tra_snt(14);
kiem_tra_snt(15);

//giải PTBN

function phuongTB1(a, b) {
  if (a === 0 && b === 0) {
    alert("Vo so nghiem");
  } else if (a !== 0 && b === 0) {
    alert("pt co nghiem x =0");
  } else if (a === 0 && a !== 0) {
    alert("pt vo nghiem");
  } else {
    alert("PT co nghiem x = " + -b / a);
  }
  return a, b;
}
console.log(phuongTB1(0, 0));

//nó sẽ kiểm tả s
function print_number() {
  // Lấy number
  var number = document.getElementById("number").value;
  // Ép number sang kiểu INT
  number = parseInt(number);
  // Lặp để in kết quả
  var html = "";
  for (var i = 1; i <= number; i++) {
    html += i + " <br/>";
  }
  document.getElementById("result1").innerHTML = html;
}

//kiểm có phải là số nguyên tố lun không

function kiem_tra_snt(n) {
  // Biến cờ hiệu
  var flag = true;

  // Nếu n bé hơn 2 tức là không phải số nguyên tố
  if (n < 2) {
    flag = false;
  } else if (n == 2) {
    flag = true;
  } else if (n % 2 == 0) {
    flag = false;
  } else {
    // lặp từ 3 tới n-1 với bước nhảy là 2 (i+=2)
    for (var i = 3; i <= Math.sqrt(n); i += 2) {
      if (n % i == 0) {
        flag = false;
        break;
      }
    }
  }

  return flag;
}

// Hàm in ra các số nguyên tố từ 1 tới n
function print_snt() {
  // Lấy number
  var number = document.getElementById("number1").value;

  // Ép number sang kiểu INT
  number = parseInt(number);

  // Lặp để in kết quả
  var html = "";
  for (var i = 1; i <= number; i++) {
    // Nếu là số nguyên tố thì in ra
    if (kiem_tra_snt(i)) {
      html += i + " <br/>";
    }
  }
  document.getElementById("result2").innerHTML = html;
}

function change_backgroud() {
  var divs = document.getElementsByTagName("div");
  for (var i = 0; i < divs.length; i++) {
    // Vị trí chẵn => màu đỏ
    if ((i + 1) % 2 == 0) {
      divs[i].style.background = "red";
    } else {
      // Vị trí lẽ => màu xanh
      divs[i].style.background = "blue";
    }
  }
}
